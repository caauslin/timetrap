import os
import shutil
import tempfile
import uuid
import pytest
from timetrap.core import TimeTrapRepository


@pytest.fixture(scope='module')
def tmp_db():
    tmp_path = tempfile.mkdtemp(prefix='timetrap_tests_')
    yield os.path.join(tmp_path, 'timetrap.db')
    shutil.rmtree(tmp_path)


def create_uuid():
    return uuid.uuid4().hex


def test_create_sheets(tmp_db):
    sheet_names = [create_uuid() for _ in range(10)]
    with TimeTrapRepository(tmp_db) as repo:
        sheet_ids = [repo.create_sheet(s) for s in sheet_names]
    with TimeTrapRepository(tmp_db) as repo:
        assert [repo.find_sheet(s) for s in sheet_names] == sheet_ids


def test_delete_sheets(tmp_db):
    sheet_names = [create_uuid() for _ in range(10)]
    with TimeTrapRepository(tmp_db) as repo:
        sheet_ids = [repo.create_sheet(s) for s in sheet_names]
    with TimeTrapRepository(tmp_db) as repo:
        for sheet_id in sheet_ids:
            repo.delete_sheet(sheet_id)
        assert all(repo.find_sheet(n) is None for n in sheet_names)


