CREATE TABLE "meta" (
    "key"    TEXT,
    "value"  TEXT,
    PRIMARY KEY("key")
);

CREATE TABLE "sheet" (
    "id"     INTEGER,
    "name"   TEXT UNIQUE,
    "status" TEXT NOT NULL DEFAULT '',
    PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "entry" (
    "id"     INTEGER,
    "sheet"  INTEGER,
    "start"  TEXT,
    "end"    TEXT,
    "note"   TEXT,
    "status" TEXT NOT NULL DEFAULT '',
    PRIMARY KEY("id" AUTOINCREMENT)
);
