import os
import pathlib


def resolve_path(path):
    """Return a normalised absolute path with user's HOME directory expanded."""
    return os.path.abspath(os.path.expanduser(path))


def ensure_dir(path, is_file=False):
    """Check if a directory exists, create if it does not."""
    if is_file:
        path = os.path.dirname(path)
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
