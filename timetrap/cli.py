import abc
import functools
import click


def make_named_pass_decorator(name):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            ctx = click.get_current_context()
            obj = ctx.obj.get(name, None)
            return ctx.invoke(f, *args, obj, **kwargs)
        return wrapper
    return decorator


class _CommandHelpTextMixin(click.Command, abc.ABC):
    """Add aliases section in help text for command & group."""

    def __init__(self, *args, **kwargs):
        self._aliases = kwargs.pop('aliases', [])
        super(_CommandHelpTextMixin, self).__init__(*args, **kwargs)

    def format_help_text(self, ctx, formatter):
        super(_CommandHelpTextMixin, self).format_help_text(ctx, formatter)
        if not self._aliases:
            return
        with formatter.section('Aliases'):
            aliases = [self.name]
            aliases.extend(self._aliases)
            formatter.write_text(', '.join(aliases))


class _Command(_CommandHelpTextMixin, click.Command):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class CommandGroup(click.Group):
    """Provide aliases support for sub-command."""

    def __init__(self, *args, **kwargs):
        self._commands_aliases = {}  # alias => command mapping
        super().__init__(*args, **kwargs)

    def command(self, *args, **kwargs):
        """Add aliases support for commands."""

        def _command(func):
            aliases = kwargs.get('aliases', [])
            if not isinstance(aliases, list) and not isinstance(aliases, tuple):
                raise ValueError('Command aliases must be a list or tuple')
            kwargs['cls'] = _Command
            cmd = super(CommandGroup, self).command(*args, **kwargs)(func)
            for alias in aliases:
                if alias in self._commands_aliases:
                    raise ValueError(f'Command alias "{alias}" is already taken by "{self._commands_aliases[alias]}"')
                self._commands_aliases[alias] = cmd.name
            return cmd

        return _command

    def group(self, *args, **kwargs):
        """Add aliases support for groups"""

        def _group(f):
            aliases = kwargs.get('aliases', [])
            if not isinstance(aliases, list) and not isinstance(aliases, tuple):
                raise ValueError('Group aliases must be a list or tuple')
            kwargs['cls'] = CommandGroup
            grp = super(CommandGroup, self).group(*args, **kwargs)(f)
            for alias in aliases:
                if alias in self._commands_aliases:
                    raise ValueError(f'Command alias "{alias}" is already taken by "{self._commands_aliases[alias]}"')
                self._commands_aliases[alias] = grp.name
            return grp

        return _group

    def get_command(self, ctx, cmd_name):
        return super().get_command(ctx, self._commands_aliases.get(cmd_name, cmd_name))
