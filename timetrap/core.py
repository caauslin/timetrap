import contextlib
import os
import sqlite3
from timetrap.util import ensure_dir, resolve_path


PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class TimeTrapException(BaseException):

    def __init__(self, message):
        super().__init__(message)


class TimeTrapSettings(object):

    def __init__(self):
        pass

    @property
    def database_path(self):
        return resolve_path('~/.timetrap/timetrap.db')


class TimeTrapRepository(object):

    def __init__(self, database_path):
        self._database_path = database_path
        self._conn = None  # type: sqlite3.Connection

    def _init_db(self):
        ensure_dir(self._database_path, is_file=True)
        if os.path.exists(self._database_path):
            return
        with open(os.path.join(PROJECT_ROOT, 'schema.sql'), mode='r') as f:
            init_script = f.read()
            with contextlib.closing(sqlite3.connect(self._database_path)) as conn:
                conn.executescript(init_script)
                conn.commit()

    def __enter__(self):
        self._init_db()
        self._conn = sqlite3.connect(self._database_path)
        self._conn.row_factory = sqlite3.Row
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._conn:
            self._conn.close()
        self._conn = None

    def get_cursor(self):
        return self._conn.cursor()

    def commit(self):
        self._conn.commit()

    def create_sheet(self, sheet_name):
        sheet_name = sheet_name.strip()
        if not sheet_name:
            raise TimeTrapException(f'Sheet name cannot be empty')
        if self.find_sheet(sheet_name) is not None:
            raise TimeTrapException(f'Sheet name {sheet_name} already exists')

        c = self.get_cursor()
        c.execute('INSERT INTO sheet (name) VALUES (?)', (sheet_name,))
        self.commit()

        return c.lastrowid

    def list_sheets(self, status=''):
        c = self.get_cursor()
        c.execute('SELECT id, name FROM sheet WHERE status=?', (status,))
        return [{'id': r['id'], 'name': r['name']} for r in c.fetchall()]

    def find_sheet(self, sheet_name):
        c = self.get_cursor()
        c.execute('SELECT id FROM sheet WHERE name=?', (sheet_name,))
        result = c.fetchone()
        return result['id'] if result else None

    def delete_sheet(self, sheet_id):
        c = self.get_cursor()
        c.execute('DELETE FROM sheet WHERE id=?', (sheet_id,))
        self.commit()

    def switch_sheet(self, sheet_id):
        pass

    def create_entry(self, start_time, sheet_id=None):
        pass

    def update_entry(self, entry_id, start_time=None, end_time=None, note=None):
        pass

    def delete_entry(self, entry_id):
        pass

    def archive_entry(self, entry_id):
        pass
